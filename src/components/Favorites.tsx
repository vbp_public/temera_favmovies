import * as React from 'react'
import { BrowserView, MobileView, isBrowser, isMobile } from "react-device-detect";
import InfiniteScroll from "react-infinite-scroll-component";

import "./../assets/scss/App.scss";
import Poster from './Poster'

interface Props {
    match: any; 
}

interface State {
    favorites: Array<any>;
    movies: Array<any>;
    loading: boolean;
    error: boolean,
};

export default class Favorites extends React.Component<Props, State> {
    
    state: State = {
        favorites: JSON.parse(localStorage.getItem('favorites')),
        movies: [],
        loading: true,
        error: false
    }
    
    componentDidMount() {
        this.fetchMovies()
    }
    fetchMovies = () => {

        this.state.favorites.forEach(favorite => {
            fetch('https://api.themoviedb.org/3/movie/'+favorite+'?api_key='+process.env.REACT_APP_MOVIE_API_KEY)
                .then(response => response.json())
                .then(response => {
                    this.setState({ movies: this.state.movies.concat(response) })
                })
                .catch(error => this.setState({ 
                    loading: false, 
                    error: true 
                }));
        }, this.setState({ 
            loading: false
        }))
    }
    loadmore = () => {}
    render () {
        if(this.state.loading) return <div className="loading"><div className="loader"></div></div>
        else return <div className="movies">
            
            <BrowserView>
                <div className="grid">
                {this.state.movies.map((movie, i) => {
                    return (<Poster key={i} title={movie.title} poster={movie.poster_path} id={movie.id} vote_average={movie.vote_average} release_date={movie.release_date} />) 
                })}
                </div>
            </BrowserView>

            <MobileView>
                <InfiniteScroll className="grid"
                    dataLength={this.state.movies.length}
                    next={this.loadmore}
                    hasMore={true}
                    loader={<h4></h4>}
                    >
                    {this.state.movies.map((movie, i) => {
                        return (<Poster key={i} title={movie.title} poster={movie.poster_path} id={movie.id} vote_average={movie.vote_average} release_date={movie.release_date} />) 
                    })}
                </InfiniteScroll>
            </MobileView>
        </div>
    }
}