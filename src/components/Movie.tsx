import * as React from 'react'
import { FaStar, FaCalendar } from 'react-icons/fa';

import "./../assets/scss/App.scss";

interface Props {
    id: string;
    match: any; 
}

interface State {
    movieid: string;
    title: string;
    poster: string;
    overview: string;
    vote_average: string;
    release_date: string;
    loading: boolean;
    error: boolean;
    favorite: boolean;
};

export default class Movie extends React.Component<Props, State> {   
    
    state: State = {
        movieid: null,
        title: null,
        poster: null,
        overview: null,
        vote_average: null,
        release_date: null,
        loading: true,
        error: false,
        favorite: false
    }

    componentDidMount() {
        if(this.props.match.params && this.props.match.params.id) {
            fetch('https://api.themoviedb.org/3/movie/'+this.props.match.params.id+'?api_key='+process.env.REACT_APP_MOVIE_API_KEY)
            .then(response => response.json())
            .then(response => {
                this.setState({ 
                    movieid: response.id,
                    title: response.title,
                    poster: response.poster_path,
                    overview: response.overview,
                    vote_average: response.vote_average,
                    release_date: response.release_date,
                    loading: false,
                    favorite: JSON.parse(localStorage.getItem('favorites')).includes(response.id)
                })
            })
            .catch(error => this.setState({ 
                loading: false, 
                error: true 
            }));
        }
    }

    handleFavorite = () => {
        let favorites = JSON.parse(localStorage.getItem('favorites'));
        if(!this.isFavorite()) favorites.push(this.state.movieid)
        else favorites = favorites.filter(favorite => favorite !== this.state.movieid)
        localStorage.setItem('favorites', JSON.stringify(favorites));
        this.setState({ favorite: this.isFavorite() })
    }
   
    isFavorite = () => {
        return JSON.parse(localStorage.getItem('favorites')).includes(this.state.movieid)
    }

    parseDate = () => {
        let date = new Date(this.state.release_date)
        return Intl.DateTimeFormat('it',{
            year: 'numeric',
            month: 'short',
            day: '2-digit' }).format(date);
    }
    
    render () {
        if(this.state.loading) return <div className="loader"></div>
        else return <div className="movie-card">
            <div className="poster">
                <img src={process.env.REACT_APP_MOVIE_POSTER_PATH+this.state.poster} />
            </div>
            <div className="metas">
                <h1 className="title">{this.state.title}</h1>
                <div className="data"><FaStar className="icon" /> {this.state.vote_average} <FaCalendar className="icon"/> { this.parseDate() } </div>
                <div className="description">{this.state.overview}</div>
                <button onClick={() => this.handleFavorite()}
                    className={"favorite " + (!this.state.favorite ? 'add' : 'remove')}
                    >{!this.state.favorite ? 'Add To Favourites' : 'Remove From Favourites'}</button>
            </div>
        </div>
    }
}