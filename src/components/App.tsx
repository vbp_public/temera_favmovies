import * as React from "react";
import { hot } from "react-hot-loader";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import Movies from "./Movies"
import Movie from "./Movie"
import Favorites from "./Favorites";

const reactLogo = require("./../assets/img/react_logo.svg");
import "./../assets/scss/App.scss";

class App extends React.Component<Record<string, unknown>, undefined> {

  componentWillMount() {
    if(!localStorage.getItem('favorites')) localStorage.setItem('favorites', JSON.stringify([]));
  }

  public render() {
    return (
      <div className="app">
        <div className="wrapper"><Router>
            <div className="header">
              <div className="left-header">
                <Link to="/">Fav<strong>Movies</strong></Link>
              </div>
              <div className="right-header">
                <Link to="/">Top Rated</Link>
                <Link to="/favorites">Favorites</Link>
              </div>
            </div>
            <Route path="/" exact component={Movies} />
            <Route path="/favorites" exact component={Favorites} />
            <Route path="/movie/:id" component={Movie} />
          </Router>
        </div>
      </div>
    );
  }
}

declare let module: Record<string, unknown>;

export default hot(module)(App);
