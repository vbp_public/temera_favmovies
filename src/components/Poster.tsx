import * as React from 'react'
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { FaStar, FaCalendar } from 'react-icons/fa';

import "./../assets/scss/App.scss";

interface Props {
    title: string;
    poster: string;
    vote_average: string;
    release_date: string;
    id: string;
}

interface State {
    loading: boolean;
    error: boolean
};

export default class Poster extends React.Component<Props, State> {    

    parseDate = () => {
        let date = new Date(this.props.release_date)
        return Intl.DateTimeFormat('it',{
            year: 'numeric',
            }).format(date);
    }

    style = () => {
        return {backgroundImage: `url(${process.env.REACT_APP_MOVIE_POSTER_PATH+this.props.poster})`}
    }

    render () {
        return <div className="poster">
            <Link to={"/movie/"+this.props.id}>
                <div className="movie-link">
                    <div className="overlay" style={this.style()}>
                        <div className="metas">
                            <div><FaStar className="icon" /> {this.props.vote_average}</div>
                            <div><FaCalendar className="icon"/> { this.parseDate() }</div>
                        </div>
                    </div>
                    <div className="title">{this.props.title}</div>
                </div>
            </Link>
        </div>
    }
}