import * as React from 'react'
import { BrowserView, MobileView, isBrowser, isMobile } from "react-device-detect";
import InfiniteScroll from "react-infinite-scroll-component";

import "./../assets/scss/App.scss";
import Poster from './Poster'

interface Props {
    match: any; 
}

interface State {
    movies: Array<any>;
    loading: boolean;
    error: boolean,
    page: number;
};

export default class Movies extends React.Component<Props, State> {
    state: State = {
        movies: [],
        loading: true,
        error: false,
        page: 1
    }
    componentDidMount() {
        this.fetchMovies()
    }
    fetchMovies = () => {
        fetch('https://api.themoviedb.org/3/movie/top_rated?api_key='+process.env.REACT_APP_MOVIE_API_KEY+'&page='+this.state.page)
            .then(response => response.json())
            .then(response => {
                this.setState({ 
                    movies: this.state.movies.concat(response.results),
                    loading: false
                })
            })
            .catch(error => this.setState({ 
                loading: false, 
                error: true 
            }));
    }
    loadmore = () => {
        this.setState({
            page: (this.state.page + 1)
        }, this.fetchMovies)
    }
    render () {
        if(this.state.loading) return <div className="loading"><div className="loader"></div></div>
        else return <div className="movies">
            
            <BrowserView>
                <div className="grid">
                    {this.state.movies.map((movie, i) => {
                        return (<Poster key={i} title={movie.title} poster={movie.poster_path} id={movie.id} vote_average={movie.vote_average} release_date={movie.release_date} />) 
                    })}
                </div>
                <button className="load-more" onClick={() => this.loadmore()}>Load more</button>
            </BrowserView>

            <MobileView>
                <InfiniteScroll className="grid"
                    dataLength={this.state.movies.length}
                    next={this.loadmore}
                    hasMore={true}
                    loader={<h4>Loading...</h4>}
                    >
                    {this.state.movies.map((movie, i) => {
                        return (<Poster key={i} title={movie.title} poster={movie.poster_path} id={movie.id} vote_average={movie.vote_average} release_date={movie.release_date} />) 
                    })}
                </InfiniteScroll>
            </MobileView>
        </div>
    }
}